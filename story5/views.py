from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Course, Assignment
from .forms import CourseForm
from datetime import datetime

# Create your views here.

def index(request):
    courses = Course.objects.all()
    context = {'courses': courses}
    return render(request, 'story5/index.html', context)

def add(request):
    form = CourseForm()
    if request.method == "POST":
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (f"Matkul {request.POST['name']} telah ditambahkan!"))
            return redirect('story5:add')
        else:
            messages.warning(request, ("Data yang Anda masukkan tidak sesuai kriteria. Harap coba lagi!"))
            return redirect('story5:add')
    context = {'form' : form}
    return render(request, 'story5/add.html', context)

def deleteItem(request, pk):
    course = Course.objects.get(id=pk)
    course.delete()
    messages.success(request, (f"Mata Kuliah {course} telah dihapus"))
    return redirect('story5:index')

def detail(request, pk):
    course = Course.objects.get(id=pk)
    assignment = Assignment.objects.filter(course = course)
    today = datetime.now().timestamp()
    passed = []
    nearDeadline = []
    farDeadline = []
    for item in assignment:
        if item.date.timestamp() < today:
            passed.append(item)
        elif item.date.timestamp() < today + 86400:
            nearDeadline.append(item)
        else:
            farDeadline.append(item)
    context = {
        'course': course, 
        'passed' : passed,
        'nearDeadline': nearDeadline,
        'farDeadline': farDeadline
        }
    return render(request, 'story5/detail.html', context)
